//
//  tddTennisTests.swift
//  tddTennisTests
//
//  Created by Joji Moreau on 02/06/2023.
//

import XCTest
@testable import tddTennis

final class tddTennisTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_TennisClassShouldExists() {
        // Given
        let tennis = Tennis()
        // Then
        XCTAssertNotNil(tennis)
    }
    
    func test_TennisGameShouldHaveAName() {
        // Given
        let tennis = Tennis()
        // When
        tennis.name = "Game 1"
        // Then
        XCTAssertEqual(tennis.name, "Game 1");
    }
    
    func test_TennisGameShouldHave2Players(){
        // Given
        let tennis = Tennis()
        // When
        tennis.player1 = Player(name: "Alice", score: 0)
        tennis.player2 = Player(name: "Bob", score: 0)
        // Then
        XCTAssertNotNil(tennis.player1)
        XCTAssertNotNil(tennis.player2)
        XCTAssertEqual(tennis.player1.name, "Alice")
    }
    
    func test_PlayerClassShouldExists() {
        // Given
        let player = Player()
        // Then
        XCTAssertNotNil(player)
    }
    
    func test_PlayerShouldHaveAName() {
        // Given
        let player = Player(name: "Alice", score: 0)
        // Then
        XCTAssertEqual(player.name, "Alice")
    }
    
    func test_PlayerShouldHaveAScore() {
        // Given
        let player = Player(name: "Alice", score: 4)
        // Then
        XCTAssertEqual(player.score, 4)
    }
    // Todo: Player should have a score
    
    func test_PlayerShouldIncreaseScore() {
        // Given
        let player = Player(name: "Alice", score: 0)
        // When
        player.increaseScore()
        // Then
        XCTAssertEqual(player.score, 1)
    }
    
    func test_PlayerShouldDecreaseScore() {
        // Given
        let player = Player(name: "Alice", score: 1)
        // When
        player.decreaseScore()
        // Then
        XCTAssertEqual(player.score, 0)
    }
    
    func test_thereShouldBeAWinner0_5(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 0),
                                player2: Player(name: "Bob", score: 5)
                            )
        // When
        let isWinner = tennis.checkForWinner()
        // Then
        XCTAssertEqual(isWinner, true)
    }
    
    func test_thereShouldBeAWinner2_2(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 2),
                                player2: Player(name: "Bob", score: 2)
                            )
        // When
        let isWinner = tennis.checkForWinner()
        // Then
        XCTAssertEqual(isWinner, false)
    }
    
    func test_thereShouldBeADeuce4_4(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 4),
                                player2: Player(name: "Bob", score: 4)
                            )
        // When
        let isDeuce = tennis.checkForDeuce()
        // Then
        XCTAssertEqual(isDeuce, true)
    }
    
    func test_thereShouldNotBeADeuce4_5(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 4),
                                player2: Player(name: "Bob", score: 5)
                            )
        // When
        let isDeuce = tennis.checkForDeuce()
        // Then
        XCTAssertEqual(isDeuce, false)
    }
    
    func test_thereIsAdvantage4_5(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 4),
                                player2: Player(name: "Bob", score: 5)
                            )
        // When
        let isAdvantage = tennis.checkForAdvantage()
        // Then
        XCTAssertEqual(isAdvantage, true)
    }
    
    func test_thereIsNoAdvantage3_5(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 3),
                                player2: Player(name: "Bob", score: 5)
                            )
        // When
        let isAdvantage = tennis.checkForAdvantage()
        // Then
        XCTAssertEqual(isAdvantage, false)
    }
    
    func test_thereIsNoAdvantage2_3(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 2),
                                player2: Player(name: "Bob", score: 3)
                            )
        // When
        let isAdvantage = tennis.checkForAdvantage()
        // Then
        XCTAssertEqual(isAdvantage, false)
    }
    
    func test_getRawScoreReturnsScore0_0(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 0),
                                player2: Player(name: "Bob", score: 0)
                            )
        // When
        let printText = tennis.getRawScore()
        // Then
        XCTAssertEqual(printText, "0 - 0")
    }
    
    func test_getRawScoreReturnsScore1_1(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 4),
                                player2: Player(name: "Bob", score: 0)
                            )
        // When
        let printText = tennis.getRawScore()
        // Then
        XCTAssertEqual(printText, "4 - 0")
    }
    
    func test_getPrettyScoreReturnsForScore1_1(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 1),
                                player2: Player(name: "Bob", score: 1)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Fifteen - Fifteen")
    }
    
    func test_getPrettyScoreReturnsForScore0_0(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 0),
                                player2: Player(name: "Bob", score: 0)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Love - Love")
    }
    
    func test_getPrettyScoreReturnsDeuceForScore3_3(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 3),
                                player2: Player(name: "Bob", score: 3)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Deuce")
    }
    
    func test_getPrettyScoreReturnsDeuceForScore4_4(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 4),
                                player2: Player(name: "Bob", score: 4)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Deuce")
    }
    
    func test_getPrettyScoreReturnsAdvantageForScore3_4(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 3),
                                player2: Player(name: "Bob", score: 4)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Advantage Bob")
    }
    
    func test_getPrettyScoreReturnsAdvantageForScore6_5(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 6),
                                player2: Player(name: "Bob", score: 5)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Advantage Alice")
    }
    
    func test_getPrettyScoreReturnsWinsForScore4_2(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 4),
                                player2: Player(name: "Bob", score: 2)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Alice wins")
    }
    
    func test_getPrettyScoreReturnsWinsForScore0_4(){
        // Given
        let tennis = Tennis (
                                name: "Randome Game",
                                player1: Player(name: "Alice", score: 0),
                                player2: Player(name: "Bob", score: 4)
                            )
        // When
        let printText = tennis.getScore()
        // Then
        XCTAssertEqual(printText, "Bob wins")
    }
    
    


}
