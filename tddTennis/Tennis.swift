//
//  TennisFunctions.swift
//  tennis-kata
//
//  Created by Joji Moreau on 15/05/2023.
//

import Foundation

let ONE_POINT = 1
let MIN_DIFF_POINTS_NEEDED_FOR_WIN = 2
let MIN_POINTS_TO_WIN = 3

class Tennis{
    var name: String
    var player1: Player
    var player2: Player
    
    init(name: String, player1: Player, player2: Player) {
        self.name = name
        self.player1 = player1
        self.player2 = player2
    }
    convenience init() {
        self.init(name: "Random Game", player1: Player(), player2: Player())
    }
    
    func checkForWinner() -> Bool {
        if(aPlayerIsLeadingWith2PointsOrMore() && aPlayerHasEnoughPointsToWin()){
            return true
        }
        return false
    }
    
    func checkForDeuce() -> Bool {
        if(bothPlayerHasEnoughPointsToWin() && player1.score == player2.score){
            return true
        }
        return false
    }
    
    func checkForAdvantage() -> Bool {
        if(bothPlayerHasEnoughPointsToWin() && aPlayerIsLeadingWith1Point()){
            return true
        }
        return false
    }
    
    func getRawScore() -> String {
        return "\(player1.score) - \(player2.score)"
    }
    
    func getScore() -> String {
        if(checkForWinner()){
            return "\(getNameOfPlayerWithHighestScore()) wins"
        }
        if(checkForDeuce()){
            return "Deuce"
        }
        if(checkForAdvantage()){
            return "Advantage \(getNameOfPlayerWithHighestScore())"
        }
        return "\(getPrettyScore(player1.score)) - \(getPrettyScore(player2.score))"
    }
    
    private func getPrettyScore(_ score: Int) -> String {
        switch score{
        case 0: return "Love"
        case 1: return "Fifteen"
        case 2: return "Thirty"
        case 3...: return "Forty"
        default: return "Error"
        }
    }
    
    private func getNameOfPlayerWithHighestScore() -> String {
        return (self.player1.score > self.player2.score ? self.player1.name : self.player2.name)
    }
    
    private func aPlayerIsLeadingWith2PointsOrMore() -> Bool {
        return abs(self.player1.score - self.player2.score) >= MIN_DIFF_POINTS_NEEDED_FOR_WIN
    }
    
    private func aPlayerIsLeadingWith1Point() -> Bool {
        return abs(self.player1.score - self.player2.score) == ONE_POINT
    }
    
    private func aPlayerHasEnoughPointsToWin() -> Bool {
        return playerHasEnoughPointsToWin(self.player1.score) || playerHasEnoughPointsToWin(self.player2.score)
    }
    
    private func playerHasEnoughPointsToWin(_ score: Int) -> Bool {
        return score >= MIN_POINTS_TO_WIN
    }
    
    private func bothPlayerHasEnoughPointsToWin() -> Bool {
        return playerHasEnoughPointsToWin(self.player1.score) && playerHasEnoughPointsToWin(self.player2.score)
    }
    
}

class Player{
    var name: String
    var score: Int
    init(name: String, score: Int) {
        self.name = name
        self.score = score
    }
    convenience init() {
        self.init(name: "Random Player", score: 0)
    }
    
    func increaseScore() {
        self.score += 1
    }
    
    func decreaseScore() {
        self.score -= 1
    }
}
