//
//  tddTennisApp.swift
//  tddTennis
//
//  Created by Joji Moreau on 02/06/2023.
//

import SwiftUI

@main
struct tddTennisApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
